extends Camera2D

var zoom_amount = 0.0005
var zoom_max = 0.8
var zoom_min = 0.6

var can_zoom := false

func _process(delta):
	if can_zoom:
		zoom.x = clamp(zoom.x - zoom_amount, zoom_min, zoom_max)
		zoom.y = clamp(zoom.y - zoom_amount, zoom_min, zoom_max)
	else:
		zoom.x = clamp(zoom.x + zoom_amount, zoom_min, zoom_max)
		zoom.y = clamp(zoom.y + zoom_amount, zoom_min, zoom_max)


func _on_Head_close_call():
	can_zoom = true

func _on_Head_close_call_ended():
	can_zoom = false
