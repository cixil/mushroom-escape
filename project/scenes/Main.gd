extends Node

func _ready():
	$Mushroom.initialize()
	Audio.play("backtrackintro")


func _on_StartButton_game_started():
	Audio.play("backtrack")
	Audio.stop("backtrackintro")
	$StartingConstraints.queue_free()
