extends Node2D


onready var shroom = $Mushroom
onready var particles = $GroundParticles
onready var orb_parts = $OrbParticles as Node2D
onready var credit_timer = $Timer as Timer
onready var credits = $Credits as Node2D

signal credits_rolled()

func _ready():
	Audio.stop("backtrack")
	Audio.play("wind")
	
	for node in credits.get_children():
		node.modulate.a = 0
	
	shroom.visible = false
	shroom.disable_camera()
	shroom.make_heppy()
	yield(get_tree().create_timer(3.5), "timeout")
	shroom.make_heppy()
	release_shroom()

func release_shroom():
	Audio.play("pop")
	particles.emitting = true
	shroom.visible = true
	yield(get_tree().create_timer(0.5), "timeout")
	for _i in range(Inventory.orbs):
		var orb_particle = orb_parts.get_child(rand_range(0,10))
		orb_particle.emitting = true
		orb_particle.get_child(0).emitting = true
		Inventory.remove_orb()
		Audio.play("orbrelease")
		yield(get_tree().create_timer(0.2), "timeout")
	shroom.make_default()
	shroom.initialize(4)
	yield(get_tree().create_timer(1), "timeout")
	credit_timer.start()

var idx = 0
func _on_Timer_timeout():
	var node = credits.get_child(idx)
	if node.modulate.a < 1:
		node.modulate.a += 0.1
	elif idx < 5:
		idx += 1
	else:
		credit_timer.stop()
		emit_signal("credits_rolled")
