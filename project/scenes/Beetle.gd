extends KinematicBody2D

onready var sprite := $AnimatedSprite

var base_speed := 70
var speed := base_speed
var velocity := Vector2()

var directions := [Vector2(1,0), Vector2(-1,0), Vector2(1,0), Vector2(-1,0), Vector2()]
var direction := Vector2()

func _physics_process(delta):
	var collision = move_and_collide(direction*speed*delta)
	if collision:
		direction.x *= -1

func _on_Timer_timeout():
	if randf() > 0.5:
		direction = directions[randi() % len(directions)]
		if direction.x < 0:
			sprite.flip_h = false
		if direction.x > 0:
			sprite.flip_h = true
		if speed != base_speed:
			speed = base_speed
		
		if direction == Vector2():
			sprite.stop()
		else:
			sprite.play()
		
	if randf() > 0.8 and speed == base_speed:
		speed += 200
	
