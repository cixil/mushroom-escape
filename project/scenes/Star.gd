extends Sprite

var time := 0.0

onready var timer := $Timer as Timer

func _ready():
	modulate.a = 0
	var s = clamp(randf(), 0.2, 0.6)
	scale = Vector2(s,s)
	yield(get_tree().create_timer(rand_range(0,4)), "timeout")
	timer.start()

func _on_Timer_timeout():
	time += 0.2
	modulate.a += sin(time)
