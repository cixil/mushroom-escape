extends Label

onready var start_timer := $StartTimer as Timer
onready var start_fade_timer := $StartFadeTimer as Timer
onready var fade_timer := $FadeTimer as Timer

func _ready():
	visible = false

func _on_StartButton_game_started():
	start_timer.start()

func _on_StartTimer_timeout():
	visible = true
	start_fade_timer.start()

func _on_StartFadeTimer_timeout():
	fade_timer.start()

func _on_FadeTimer_timeout():
	if modulate.a > 0:
		modulate.a -= 0.1
	else:
		queue_free()
