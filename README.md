# Mushroom Awakening

Source code for a game submitted to Godot Wild Jam 44.
Art was made in Aseprite (.aseprite fiels) and GNU Image Manipulation Program (.xcf files)

Play the game here! [https://ccpixel.itch.io/mushroom](https://ccpixel.itch.io/mushroom)
