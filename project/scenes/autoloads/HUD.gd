extends CanvasLayer

onready var orb_panel := $OrbAmountPanel as Panel
onready var orb_amt := $OrbAmountPanel/Amount as Label
onready var game_over_screen = $GameOver
onready var black_screen := $Black as ColorRect
onready var fade_out_timer := $Black/FadeOutTimer as Timer
onready var fade_in_timer := $Black/FadeInTimer as Timer

var game_is_over:bool

func _ready():
	var _conn = Inventory.connect("orb_added", self, "_update_orb_amount")
	reset()

func reset():
	black_screen.visible = true
	black_screen.modulate.a = 0
	orb_panel.visible = false
	game_over_screen.visible = false
	game_is_over = false

func _update_orb_amount():
	if Inventory.orbs > 0:
		orb_panel.visible = true
	else:
		orb_panel.visible = false
	orb_amt.text = str(Inventory.orbs)

func fade_to_black():
	fade_out_timer.start()

func unfade_black():
	fade_in_timer.start()

func game_over():
	game_over_screen.visible = true
	yield(get_tree().create_timer(0.4), "timeout")
	game_is_over = true


func _input(event):
	if game_is_over and event is InputEventKey:
		if event.pressed:
			GameControl.restart_game()

func _on_FadeOutTimer_timeout():
	if black_screen.modulate.a < 1:
		black_screen.modulate.a += 0.05
	else:
		fade_out_timer.stop()

func _on_FadeInTimer_timeout():
	if black_screen.modulate.a > 0:
		black_screen.modulate.a -= 0.05
	else:
		fade_in_timer.stop()
