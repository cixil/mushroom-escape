extends Area2D

func _on_SporeCollectable_body_entered(body):
	if body.name == "Head":
		Audio.play("coin")
		Inventory.add_spore()
		
#		sprite.visible = false
#		particles.visible = false
#		explosion_particles.emitting = true
#		yield(get_tree().create_timer(1), "timeout")
		queue_free()
