extends Node

func reset():
	stop("backtrack")
	stop("wind")
	stop("backtrackintro")

func play(sound_name:String):
	if has_node(sound_name):
		get_node(sound_name).play()
	else:
		print_debug("Sound not found: ", sound_name)

func stop(sound_name:String):
	if has_node(sound_name):
		get_node(sound_name).stop() 
	else:
		print_debug("Sound not found: ", sound_name)
