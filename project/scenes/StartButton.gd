extends Area2D

var time = 0
signal game_started()

func _on_StartButton_body_entered(body):
	if body.name == "Head":
		Audio.play("start")
		emit_signal("game_started")
		queue_free()



func _on_Timer_timeout():
	time += 0.5
	position.y += sin(time)
