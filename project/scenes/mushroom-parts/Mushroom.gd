class_name Mushroom extends Node2D

onready var head = $Head as RigidBody2D
onready var stem_base = $StemSegment
var layers
var gravity

signal _children_translucent()

func _ready():
	layers = head.layers
	head.layers = 0
	gravity = head.gravity_scale
	head.gravity_scale = 0

func initialize(link_limit=20):
	head.enable(link_limit)
	head.layers = layers
	visible = true

func activate():
	head.game_started = true
	initialize()

func disable_camera():
	head.camera.current = false

func enable_camera():
	head.camera.current = true

func make_heppy():
	head.make_heppy()

func make_default():
	head.make_default()

func _on_Head_exploded():
	translucify_children(head)
	yield(self, "_children_translucent")
	translucify_children(stem_base)
	yield(get_tree().create_timer(10), "timeout")
	queue_free()

func translucify_children(parent:Node):
	var amount = parent.get_child_count()
	for i in range(amount-1, 0, -1):
		var node = parent.get_child(i)
		if node.is_in_group("stems"):
			node.modulate.a = 0.2
			yield(get_tree().create_timer(0.05), "timeout")
	emit_signal("_children_translucent")


func _on_StartButton_game_started():
	head.game_started = true
