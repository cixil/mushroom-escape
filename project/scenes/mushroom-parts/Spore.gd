extends RigidBody2D

var ready := false
onready var shroom := $Mushroom
onready var sprite := $AnimatedSprite as AnimatedSprite

func _ready():
	shroom.visible = false

func _process(_delta):
	if ready and abs(linear_velocity.x) + abs(linear_velocity.y) < 0.05:
		ready = false
		mode = RigidBody2D.MODE_STATIC
		
		sprite.frame = 1
		
		yield(get_tree().create_timer(1.5), "timeout")
		Audio.play("emergence")
		yield(get_tree().create_timer(0.5), "timeout")
		shroom.activate()
		
		if has_node("Camera2D"):
			$Camera2D.current = false
		shroom.enable_camera()
	
	if mode == RigidBody2D.MODE_STATIC:
		pass
	
func _on_Timer_timeout():
	ready = true
