class_name MushroomHead extends RigidBody2D

onready var camera = $Camera2D as Camera2D
onready var sprite = $BaseHead as AnimatedSprite
onready var faces = $Faces as AnimatedSprite
onready var particles = $Particles as Node
onready var hitbox = $Hitbox as Area2D
onready var spore_particles = $Spores as CPUParticles2D
onready var idle_timer = $IdleTimer

const SPORE = "res://scenes/mushroom-parts/Spore.tscn"
const LINK = preload("res://scenes/mushroom-parts/StemPieceShort.tscn")
const CONNECTOR = preload("res://scenes/mushroom-parts/StemPieceConnector.tscn")

# parameters
var force_base_strength := 50
var force_limit := 1500
var force_down_limit := 500

# state
var force_dir := Vector2()
var highest_cam_pos := Vector2(INF, INF)
var exploded := false
var adding_link := false
var prev_connector:PinJoint2D
var movement_enabled := false
var length := 0
var game_started := false
var initial_link_limit := 20

signal exploded()
signal close_call()
signal close_call_ended()

func _ready():
	spore_particles.visible = false
	if Inventory.spores > 0 :
		spore_particles.visible = true
	
	for _i in range(Inventory.spores):
		add_spore()
	var _conn = Inventory.connect("spore_added", self, "add_spore")
	# depending on parents like this is bad form but I'm lazy
	prev_connector = get_parent().get_node("StemSegment/StemPieceConnector")
	add_link()
	applied_force.y = -100

func enable(link_limit=initial_link_limit):
	movement_enabled = true
	initial_link_limit = link_limit

func _process(_delta):
	if exploded or !movement_enabled:
		return
	
	if Input.is_action_just_pressed("ui_left") or Input.is_action_just_pressed("ui_right") or \
		Input.is_action_just_pressed("ui_up") or Input.is_action_just_pressed("ui_down"):
			play_default_face()
	
	force_dir = Vector2()
	if Input.is_action_pressed("ui_up"):
		force_dir.y = -1
	if Input.is_action_pressed("ui_down"):
		force_dir.y = 1
	if Input.is_action_pressed("ui_left"):
		force_dir.x = -1
	if Input.is_action_pressed("ui_right"):
		force_dir.x = 1
	force_dir = force_dir.normalized()
	
	# Stop applying force if buttons are released
	if force_dir.x == 0:
		applied_force.x = 0
	if force_dir.y == 0:
		applied_force.y = 0
	
	# apply forces based on buttons pressed
	if abs(applied_force.x + force_dir.x*force_base_strength) < force_limit:
		add_force(Vector2(), Vector2(force_dir.x*force_base_strength, 0))
	if (force_dir.y < 0 and abs(applied_force.y) < force_limit) or \
	   (force_dir.y > 0 and applied_force.y < force_down_limit):
		add_force(Vector2(), Vector2(0, force_dir.y*force_base_strength))
	
	# Add links as mushroom grows
	if !adding_link and (abs(applied_force.x)+50 >= force_limit or abs(applied_force.y) >= force_limit):
		if game_started or length < initial_link_limit:
			length += 1
			adding_link = true
			add_link()

func add_link():
	var link = LINK.instance()
	var connector:PinJoint2D = CONNECTOR.instance()
	add_child(link)
	add_child(connector)
	connector.node_a = self.get_path()
	connector.node_b = link.get_path()
	prev_connector.node_a = link.get_path()
	link.z_as_relative = false
	link.z_index = 0
	
	var m = 5
	link.global_position = Vector2(global_position.x + m*force_dir.x, global_position.y + m*force_dir.y)
	link.global_position.y += m
	prev_connector = connector
	yield(get_tree().create_timer(0.1), "timeout")
	adding_link = false

func add_spore():
	if spore_particles.amount == 1 and spore_particles.visible == false:
		spore_particles.visible = true
	else:
		spore_particles.amount += 1

func remove_spore():
	if spore_particles.amount > 1:
		spore_particles.amount -= 1
	else:
		spore_particles.visible = false

func explode():
	exploded = true
	emit_signal("exploded")
	hitbox.collision_layer = 0
	hitbox.collision_mask = 0
	applied_force = Vector2()
#	layers = 0
	
	spore_particles.visible = false
	sprite.visible = false
	faces.visible = false
	Audio.play("boom")
	for emitter in particles.get_children():
		emitter.emitting = true
	
	if Inventory.spores > 0:
		eject_spore()
	else:
		GameControl.game_over()
	
func eject_spore():
	Inventory.spores -= 1
	var spore_scene = load(SPORE)
	var spore = spore_scene.instance()
	get_tree().root.call_deferred("add_child", spore)
	spore.global_position = global_position
	remove_child(camera)
	spore.add_child(camera)

func play_default_face():
	if faces.animation == "derp":
		faces.animation = "default"
		derping = false

func _on_Hitbox_body_entered(body):
	if !exploded and movement_enabled and body.is_in_group("obstacles"):
		exploded = true
		layers = 0
		explode()

func _on_CloseCallDetector_body_entered(body):
	if body.is_in_group("obstacles"):
		faces.animation = "close_call"
		emit_signal("close_call")

func _on_CloseCallDetector_body_exited(body):
	if body.is_in_group("obstacles"):
		faces.animation = "default"
		emit_signal("close_call_ended")

func make_heppy():
	faces.animation = "heppy"
	derping = true # disable derping

func make_default():
	faces.animation = "default"

var derping := false
func _on_BlinkTimer_timeout():
	if randf() > 0.3 and faces.animation == "default" or faces.animation == "blink":
		faces.play("blink")
	if !derping and force_dir == Vector2():
		derping = true
		idle_timer.start()

func _on_IdleTimer_timeout():
	if force_dir == Vector2():
		faces.play("derp")
		faces.playing = true
