extends Area2D

var time := 0.0
onready var explosion_particles = $Explosion as CPUParticles2D
onready var particles = $CPUParticles2D as CPUParticles2D
onready var sprite = $Sprite

func _on_MagicSphere_body_entered(body):
	if body.name == "Head":
		Audio.play("coin")
		Inventory.add_orb()
		sprite.visible = false
		particles.visible = false
		explosion_particles.emitting = true
		yield(get_tree().create_timer(1), "timeout")
		queue_free()


func _on_Timer_timeout():
	position.y += sin(time)
	time += 0.5
	
