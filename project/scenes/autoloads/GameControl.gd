extends Node


func game_won():
	Hud.fade_to_black()
	yield(get_tree().create_timer(2), "timeout")
	var _x = get_tree().change_scene_to(load("res://scenes/GreatOutdoors.tscn"))
	Hud.unfade_black()

func game_over():
	yield(get_tree().create_timer(2), "timeout")
	Hud.game_over()

func restart_game():
	Inventory.reset()
	Hud.reset()
	Audio.reset()
	var _x = get_tree().change_scene_to(load("res://scenes/Main.tscn"))
