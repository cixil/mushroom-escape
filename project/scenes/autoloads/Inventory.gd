extends Node

var orbs:int
var spores:int 

signal spore_added()
signal orb_added()

func _ready():
	reset()

func reset():
	orbs = 0
	spores = 0

func add_spore():
	spores += 1
	emit_signal("spore_added")

func add_orb():
	orbs += 1
	emit_signal("orb_added")

func remove_orb():
	orbs -= 1
	emit_signal("orb_added")
