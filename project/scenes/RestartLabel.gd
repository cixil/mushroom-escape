extends Node2D

onready var timer = $Timer as Timer

func _ready():
	visible = false

func _process(_delta):
	if visible:
		if Input.is_action_just_released("enter"):
			GameControl.restart_game()

func _on_GreatOutdoors_credits_rolled():
	timer.start()

func _on_Timer_timeout():
	visible = true
